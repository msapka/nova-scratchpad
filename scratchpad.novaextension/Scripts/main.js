
exports.activate = function() {
    // Do work when the extension is activated
}

exports.deactivate = function() {
    // Clean up state before the extension is deactivated
}

const filename = (extension) => {
    const filename = nova.config.get('scratchpad.mdFilename') || "nova_scratchpad"
    const directory =  nova.config.get('scratchpad.directory') || nova.environment.HOME

    return `${directory}/${filename}.${extension}`
}

typeToExtension = (type) => {
  if(type === "CSS") return "css"
  if(type === "Javascript") return "js"
  if(type === "JSON") return "json"
  if(type === "Markdown") return "md"
  if(type === "Perl") return "pl"
  if(type === "Python") return "py"
  if(type === "Ruby") return "rb"
  if(type === "Shell script'") return "sh"
  if(type === "SQL") return "sql"
  if(type === "YML") return "yml"
  if(type === "XML") return "xml"

  return ""
}

openScratchpad = (type) => {
  console.log("Opening scratchad: " + filename())
  nova.workspace.openFile(filename(typeToExtension(type)))
}

const defaultScratchpad = () => {
  return `Markdown`
}

enabledScratchpads = () => {
  let scratchpads = [defaultScratchpad()]
  if(nova.config.get('scratchpad.cssEnabled')) scratchpads.push('CSS')
  if(nova.config.get('scratchpad.javascriptEnabled')) scratchpads.push('Javascript')
  if(nova.config.get('scratchpad.jsonEnabled')) scratchpads.push('JSON')
  if(nova.config.get('scratchpad.markdownEnabled')) scratchpads.push('Markdown')
  if(nova.config.get('scratchpad.perlEnabled')) scratchpads.push('Perl')
  if(nova.config.get('scratchpad.phpEnabled')) scratchpads.push('PHP')
  if(nova.config.get('scratchpad.pythonEnabled')) scratchpads.push('Python')
  if(nova.config.get('scratchpad.rubyEnabled')) scratchpads.push('Ruby')
  if(nova.config.get('scratchpad.shellEnabled')) scratchpads.push('Shell script')
  if(nova.config.get('scratchpad.sqlEnabled')) scratchpads.push('SQL')
  if(nova.config.get('scratchpad.ymlEnabled')) scratchpads.push('YML')
  if(nova.config.get('scratchpad.xmlEnabled')) scratchpads.push('XML')

  return scratchpads
}

showListOfScratchpads = () => {
    // openScratchpad('md')

    nova.workspace.showChoicePalette(
      enabledScratchpads(),
      {},
      openScratchpad
    )
}

nova.commands.register("scratchpad.openScratchpad", (editor) => {
  showListOfScratchpads()
});



